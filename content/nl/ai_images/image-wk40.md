---
title: 'Wolkbreuk in New York'
date: 2023-10-02
tags: ['AI']
thumbnail: /AI-image-wk/newyork.png
---

Het regent de laatste heftig in New York. [NBC News](https://www.nbcnews.com/science/environment/nyc-flooding-climate-change-infrastructure-limitations-rcna118170)
Zelfs de fakkel van het beroemde Vrijheidsbeeld is vervangen door een paraplu. Maar de paraplu kan de storm nauwelijks weerstaan.

![newyork](/AI-image-wk/newyork.png)

