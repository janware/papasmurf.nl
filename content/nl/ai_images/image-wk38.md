---
title: 'Hondekop 2.0'
date: 2023-09-24
tags: ['AI']
thumbnail: /AI-image-wk/doghead1.png
---
In de vorige eeuw had Nederland treinen die vanwege de vorm van de voorkant 'hondekop' werden genoemd.
Ik was onlangs op een tentoonstelling waar onder de naam 'Hondekop 2.0' een nieuwe serie treinstellen werd getoond.

<div style="display: flex; justify-content: space-between;">
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead1.png" id="zoom-default" class="medium-zoom-image" aalt="Image 1" style="max-width: 100%; height: auto; display: block;">
    </div>
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead2.png" id="zoom-default" class="medium-zoom-image" aalt="Image 1" style="max-width: 100%; height: auto; display: block;">
    </div>
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead3.png" id="zoom-default" class="medium-zoom-image" aalt="Image 1" style="max-width: 100%; height: auto; display: block;">
    </div>
</div>

De toenmalige Hondekop:
![hondekop](/images/hondekop.jpg) 
