---
title: 'De Cloud: het is gewoon de computer van je buurman?'
date: 2023-11-12
tags: ['AI']
thumbnail: /AI-image-wk/cloud.png
---

![cloud](/AI-image-wk/cloud.png)

"De cloud bestaat niet. Het is gewoon de computer van iemand anders."
Dat wordt vaak gezegd om 'de cloud' te verduidelijk.
Aan de ene kant is dit waar; het is in wezen een verzameling computers in datacebtra over de hele wereld.
Maar het is ook mijn eigen computer op zolder.
Aan de andere kant is de cloud een concept dat verder gaat dan alleen het lenen van de computer van je buurman.
De cloud is als een tunnel, een toeganspoort tot krachtige, gedeelde computerkracht,
waardoor je data kunt gebruiken en opslaan ver voorbij de grenzen van je eigen hardware.
