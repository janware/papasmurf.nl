---
title: 'Verkiezingen 2023'
date: 2023-11-22
tags: ['AI']
thumbnail: /AI-image-wk/elections.png
---

![elections](/AI-image-wk/elections.png)

Vandaag is het verkiezingsdag in Nederland.
Het land lijkt meer verdeeld dan ooit tevoren.
Zoveel partijen... en niet minder dan vier partijen kunnen de grootste worden met slechts enkele stemmen verschil.
Het debat op de televisie is meer een machtsstrijd tussen links en rechts dan een discussie over de inhoud.
Hopelijk wordt er vanaf morgen weer samengewerkt in plaats van verdeeldheid gezaaid.
