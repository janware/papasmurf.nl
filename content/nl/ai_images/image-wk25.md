---
title: 'Gratis hotdogs voor de Moskovieten'
date: 2023-06-25
tags: ['AI']
thumbnail: /AI-image-wk/hotdogs.png
---

De inwoners van Moskou hebben komende maandag vrij, aanvankelijk omdat het leger van de voormalige hotdogverkoper, Prigozjin, op weg was naar de stad. Deze dreiging lijkt nu te zijn afgewend. Maar maandag blijft een vrije dag. Alle burgers worden uitgenodigd naar het Rode Plein te komen voor een gratis hotdog.

![hot dogs](/AI-image-wk/hotdogs.png)
