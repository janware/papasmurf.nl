---
title: 'Messi naar Miami Beach'
date: 2023-06-09
tags: ['AI']
thumbnail: /AI-image-wk/poor-messi.png
---

Lionel Messi verkiest het bouwen zandkastelen op Miami Beach boven de gouden bergen van Saudi-Arabië. Een schamel loontje van zo'n 120 miljoen euros was genoeg om hem naar de Sunshine State te lokken.

![Poor Messi](/AI-image-wk/poor-messi.png)
