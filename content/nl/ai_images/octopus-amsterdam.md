---
title: 'Octopus fietst in Amsterdam'
date: 2023-10-04
tags: ['AI']
thumbnail: /AI-image-wk/octopus.jpeg
---

Totnogtoe gebruikte ik Midjourney om afbeeldingen te maken.
Vandaag heb ik de nieuwe DALL-E via Bing uitgeprobeerd.
'Teken een octopus die fietst in Amsterdam,' vroeg ik.
Grappig dat het een molen, een Nederlandse vlag en tulpen tekent om duidelijk te maken dat het zich in een Nederlandse stad bevindt. 🌷🇳🇱

![octopus](/AI-image-wk/octopus.jpeg)
