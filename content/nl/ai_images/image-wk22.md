---
title: 'Running for President'
date: 2023-06-04
tags: ['AI']
thumbnail: /AI-image-wk/running-for-president.png
---
Afgelopen week was Joe Biden in het nieuws omdat hij [struikelde](https://www.bbc.com/news/world-us-canada-65783589) toen hij het podium opkam. De reactie van Donald Trump was verrassed aardig: "Ik hoop dat hij zich niet bezeerd heeft". In elk geval zijn ze allebei nog in de race. Ik heb deze foto gemaakt met MidJourney. Ik heb hem de titel 'Running for President' gegeven.

![Running for President](/AI-image-wk/running-for-president.png)
