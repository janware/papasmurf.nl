---
title: 'Papa Smurf is op vakantie'
date: 2023-07-15
tags: ['AI']
thumbnail: /AI-image-wk/papasmurf-campsite.png
---

Er is heel wat gebeurd in de Nederlandse politiek sinds het vorige AI plaatje.
Het kabinet viel, en tal van prominente politici kondigen aan de politiek te gaan verlaten.
Genoeg materiaal voor een politieke cartoon.
Maar weet je wat? Papa Smurf is op vakantie! Dat vind ik op dit moment veel relevanter...
Ik zit op een camping in Noord-Frankrijk. Relaxed. Glaasje wijn erbij. En de laptop natuurlijk, want er moet ook gespeeld worden natuurlijk. 

![papasmurf-campsite](/AI-image-wk/papasmurf-campsite.png)
