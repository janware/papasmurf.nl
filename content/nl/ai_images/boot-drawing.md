---
title: 'Wonen in een laars'
date: 2023-11-05
tags: ['AI']
thumbnail: /AI-image-wk/boot-drawing.png
---

![jigsaw](/AI-image-wk/puzzel.png)

Meer dan dertig jaar geleden maakte ik deze legpuzzel voor één van mijn kinderen.
Met een tekening die ik zelf had gemaakt.

Zojuist heb ik een dergelijke tekening gemaakt met AI. Klaar in een paar minuutjes. 

![boot](/AI-image-wk/boot-drawing.png)
