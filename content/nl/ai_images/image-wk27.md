---
title: 'Meltdown...'
date: 2023-07-06
tags: ['AI']
thumbnail: /AI-image-wk/earth-melting.png
---

Deze week hadden we weer een tragisch record in de klimaatverandering: [de heetste dag](https://www.washingtonpost.com/climate-environment/2023/07/05/hottest-day-ever-recorded/) die ooit werd geregistreerd, wereldwijd. Terwijl wij van onze ijsjes likken, smelt de wereld als datzelfde bolletje ijs...

![melting earth](/AI-image-wk/earth-melting.png)
