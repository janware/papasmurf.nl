---
title: 'De Eiffeltoren huilt bloed'
date: 2023-07-01
tags: ['AI']
thumbnail: /AI-image-wk/bleeding-eiffeltower.png
---

Afgelopen week was de dood van Nahel een nieuw dieptepunt in de geschiedenis van racisme in Frankrijk. Onder een donkere hemel druipt het bloed van de Eiffeltoren.

![eiffeltower](/AI-image-wk/bleeding-eiffeltower.png)
