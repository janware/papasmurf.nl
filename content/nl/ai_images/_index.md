---
title: Papa Smurf's AI plaatjes
---
Als een enthousiaste gebruiker van Midjourney en DALL-E, heb ik bedacht om geregeld een plaatje te publiceren dat ik met AI heb gemaakt. 
Soms zal het een plaatje worden naar aanleiding van het nieuws. Grappig of kritisch, of allebei. 
Andere keren gewoon het resultaat van experimenten met AI-tools.
