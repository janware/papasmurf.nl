---
title: 'Boris Johnson''s nieuwe baan'
date: 2023-06-15
tags: ['AI']
thumbnail: /AI-image-wk/boris-johnson.png
---

Nadat ze hem uit het parlement hadden gegooid, vond Boris Johnson de dag erna meteen al een nieuwe baan als columnist voor de Daily Mail. Waarom heeft hij dat niet eerder gedaan? Hij is goed in het schrijven van grappige stukjes. Dat kan een stuk minder kwaad dan een clownesk project als de Brexit.

![Boris Johnson](/AI-image-wk/boris-johnson.png)
