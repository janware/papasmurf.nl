---
title: APL - A Programming Language
date: 2024-03-01
tags: ['Programmeren']
thumbnail: /thumbnails/aplkbd.jpg
---
Vandaag werd ik herinnerd aan één van de eerste programmeertalen die leerde in de jaren zeventig: APL.

Een collega van Visma in Riga, Letland, stuurde me vanochtend een berichtje: 
"Hey Jan, ik las ergens dat jij APL kent. Zou je ons kunnen helpen met een stukje
software dat in de jaren zeventig in APL is geschreven? Het is nog steeds operationeel
en we hebben er wat vragen over." Wow... dat is nog eens legacy software!

Maar inderdaad, ik leerde APL tijdens mijn wiskundestudie in de jaren zeventig.
Ik schreef mijn eerste regels APL op een [papierterminal](/images/about/dec-terminal.jpg).

APL staat letterlijk voor 'A Programming Language'. Een simpele naam die in schril
contrast staat met hoe het eruit ziet, een beetje als [hiërogliefen](/images/apl.png),
en het toetsenbord is ook niet eenvoudig, met APL-tekens zelfs [aan de zijkant
van de toetsen](/thumbnails/aplkbd.jpg).

Oorspronkelijk rond 1960 ontworpen als een wiskundig notatiesysteem, met een sterke
focus op het werken met multidimensionale arrays, bleek het al snel geschikt
als programmeertaal. Eén van de sterke punten is zijn beknoptheid. 
De beroemde zeef van Erastothenes om priemgetallen te genereren, kun je in een [korte
regel APL schrijven](https://old.aplwiki.com/SieveOfEratosthenes), terwijl je daar
in talen als C of Python al gauw tientallen regels voor nodig hebt.
Geen wonder dat ik er snel aan verslaafd raakte. Daardoor was al snel niet meer in 
de collegezaal te vinden, maar was ik met APL aan het stoeien in het computercentrum
van de universiteit.

Mijn wiskundestudie heb ik nooit afgemaakt. Maar doordat ik deze programmeertaal
kende kreeg ik mijn eerste baan bij de ABN Bank in Amsterdam, als APL-programmeur.
En zo begon mijn carrière in de IT.
