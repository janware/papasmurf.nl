---
title: AI versus expertise en intuïtie
date: 2023-06-01
tags: ['ChatGPT','AI']
thumbnail: /thumbnails/ai_intuition.png
---
ChatGPT en andere AI tools zijn niet meer weg te denken. De toepassingen zijn legio. In mijn werk als software-ontwikkelaar gebruik ik ze dagelijks. De oplossingen en suggesties waar deze tools mee komen, zijn indrukwekkend. Maar we moeten wel blijven beseffen: geen enkel hulpmiddel kan onze eigen ervaring en inzicht vervangen.

Vandaag moest ik een probleem oplossen van een SQL-script. Het deed weliswaar wat het doen moest, maar veel te langzaam. Zo traag als schildpad. Ik legde het probleem voor aan ChatGPT en vroeg om suggesties. ChatGPT kwam met diverse oplossingen, maar het hielp me net echt verder. Sommige werkten gewoon niet, en andere hadden niet het gewenste effect. Het script bleef gewoon langzaam.

Uiteindelijk schoof ik ChatGPT terzijde, stroopte mijn mouwen op, en ging aan het werk. Ik heb tientallen jaren ervaring, dus dat probleem moest te kraken zijn. En inderdaad, na een uurtje sleutelen was het script tienmaal zo snel als voorheen.

Deze ervaring bevestigde nog maar eens: eigen expertise en intuitie zijn nog altijd onontbeerlijk. Kunstmatige intelligentie is een prachtig hulpmiddel en kan ons veel werk uit handen nemen. Maar we moeten er niet teveel op vertrouwen. We blijven zelf de baas!
