---
title: Croc - bestanden overzetten naar andere systemen
date: 2022-12-22
tags: ['Utilities']
thumbnail: /thumbnails/croc.jpg
---
Ik ben vast niet de enige die af en toe een bestand wil kopiëren naar een ander systeem, of even een bestand wil delen met een collega.

Je kunt natuurlijk tools gebruiken als scp of winscp. Bij Windows remote desktop kun je soms bestanden tussen de system knippen en plakken. Maar afhankelijk van de situatie moet je weten hoe je bestanden kopieert voor de specifieke verbinding die je gebruikt.

En daarom ben ik een fan van Croc. Croc kan bestanden overzetten tussen systemen onafhankelijk van de soort verbinding, en zelfs als de systemen achter een firewall zitten. Ook werkt het op Windows, Linux en Mac. Het werkt met grote bestanden en zelfs met hele folders. Het enige dat je nodig hebt is Croc op beide systemen.

Geen zorgen meer over welke mogelijkheden beschikbaar zijn om bestanden te kopiëren. Denk alleen maar: Croc! Ik gebruik het in mijn thuis-netwerk om bestanden tussen laptops te kopiëren. Ik gebruik op m'n werk om bestande naar de cloud over te zetten, of tussen twee systemen in de cloud.

Op het systeem waar je te kopiëren bestand staat, typ je:
```
croc <filename>
```

Er komt een melding in de trant van: On the other computer run: croc 3103-magnet-person-mental (iedere keer een andere code)

Then on the computer getting the file, all you do is type in that command: 
```
croc 3103-magnet-person-mental
```

Dat is alles. Zo simpel is het om bestanden te kopiëren tussen twee willekeurige systemen. Een kind kan de was doen!

Voor meer instructies en informatie ga je naar https://github.com/schollz/croc.
