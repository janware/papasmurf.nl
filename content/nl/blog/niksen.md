---
title: Niksen - Een typisch Nederlands woord
date: 2024-02-15
tags: ['Taal']
thumbnail: /thumbnails/niksen.avif
---
Het Nederlands blijkt de enige taal te zijn met een woord voor niets doen: niksen.

Ik kwam hier achter door [dit artikel](https://www.theguardian.com/lifeandstyle/2024/feb/07/the-art-of-doing-nothing-have-the-dutch-found-the-answer-to-burnout-culture).

In het artikel onderzoekt Viv Groskop het Nederlandse concept niksen, oftewel het actief nietsdoen, als mogelijke oplossing voor de burn-outcultuur. 
Zij beschrijft haar ervaring met niksen op het strand van Scheveningen, samen met Olga Mecking, auteur van het boek "Niksen: Embracing the Dutch Art of Doing Nothing". 
Mecking, die gezien wordt als een autoriteit op het gebied van niksen, definieert het als iets doen zonder doel, een concept dat internationaal veel aandacht heeft getrokken. 
Ondanks de populariteit en de publicatie van diverse boeken over het onderwerp, 
is niksen niet inherent Nederlands noch diep geworteld in de Nederlandse cultuur, 
die traditioneel hard werken en productiviteit waardeert, in lijn met de calvinistische volksaard.

Het artikel belicht ook de bredere context van burn-out en stress in de moderne samenleving, 
waarbij niksen wordt voorgesteld als een manier om te ontsnappen aan de druk van een steeds veeleisendere wereld. 
De interesse in niksen is deels een reactie op de moderne levensstijl, waarbij mensen constant bezig zijn en weinig tijd hebben voor ontspanning. 
Hoewel het concept recentelijk veel aandacht heeft gekregen, 
waarschuwen experts dat het geen wondermiddel is en dat de effectiviteit ervan afhangt van de individuele benadering van ontspanning en vrije tijd.
