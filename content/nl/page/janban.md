---
title: Outlook KanBan board aka JanBan
date: 2017-10-02
aliases: ['/janban']
---
![logo](/images/janban/janban-icon-512-300x300.jpg)

[Download link](http://www.janware.nl/gitlab/janban.zip)

Het JanBan Outlook Taskboard is een kanban board voor Outlook taken.

Dit project heeft [Outlook Taskboard van Evren Varol](https://github.com/evrenvarol/outlook-taskboard) als basis. Ik heb daar talloze features en een configuratie-scherm aan toegevoegd.

![](/images/janban/scr1.png)

De belangrijkste features van JanBan zijn:
##### Het verplaatsen van taken naar andere kolommen
![](/images/janban/scr2.gif)
##### Taken filteren
![](/images/janban/scr3.gif)
##### Configuratie
![](/images/janban/scr4.gif)
##### Support Mailbox
![](/images/janban/scr5.gif)

### Ondersteunde platforms

Ik heb het getest met alle Outlook versies vanaf 2013, en op Windows versie 8 tot 11. Waarschijnlijk werkt het ook met oudere versies van Outlook en ook op Windows 7.

Het is ook mogelijk het taakbord direct in de browser te openen. Maar omdat het ActiveX gebruikt, kan dit alleen met de Internet Explorer browser.


### Setup mogelijkheden

JanBan kan vanaf internet online worden gebruikt, maar het kan ook lokaal op de eigen computer worden geïnstalleerd.

Onafhankelijk van de manier waarop het worden gedraaid, het zal altijd alleen actief zijn binnen de lokale Outlook applicatie, waardoor informatie alleen binnen Outlook beschikbaar is en nooit daarbuiten.

Om te kunnen beslissen hoe je het wilt installeren hier een overzicht van de voor- en nadelen per optie:

- Standaard:
  - Setup: Zeer eenvoudig
  - Updates: Gaan volledig automatisch. Daar hoef je niets voor te doen.
  - Offline: De app zal niet werken zonder internetverbinding.

- Lokale installatie:
  - Setup: Vergt wat eigen handmatig werk.
  - Updates: Moet je zelf installeren.
  - Offline: Je kunt de app ook gebruiken zonder internetverbinding.

### Standaard setup.

Ga je voor het gemak, ga dan naar https://janware.nl/janban en volg de instructies.

Als je kiest voor de handmatige installatie:

1. Rechtsklik op de Outlook Home Folder, en klik dan op 'Properties'.
2. Ga naar de _Home Page_ tab in het volgende scherm.
3. In het veld met label _Address_, type je: https://janware.nl/janban.
4. Zet in een vinkje in de checkbox _Show home page by default for this folder_ en klik op OK.

### Lokale setup

1. Download de zip file van [JanWare](http://www.janware.nl/gitlab/janban.zip), unzip het en sla de bestanden op op je computer.
2. Ga naar de _Home Page_ tab in het volgende scherm.
3. Choose the _Home Page_ tab in the box that pops up.
4. In het veld met label _Address_, zoek de folder waar je de bestanden hebt opgeslagen en kies het bestand __kanban.html__.
4. Zet in een vinkje in de checkbox _Show home page by default for this folder_ en klik op OK.

![](/images/janban/scr6.png)

Mocht je deze waarschuwing zien, dan kun je die negeren. Gewoon op OK klikken.

### Installeren met de cmd file in het zip bestand

Download het bestand via de download link op deze pagina naar je Downloads folder.

![](/images/janban/setup1.png)

Ga naar de Downloads folder, rechtsklik op janban.zip en kies voor Extract All.

![](/images/janban/setup2.png)

Kies ervoor om de uitgepakte bestanden te zien, en klik op de Extract knop.

![](/images/janban/setup3.png)

Dubbelklik het install.cmd bestand. Er kan kort een zwart command scherm te zien zijn.

![](/images/janban/setup4.png)

Als er een scherm komt met een beschermings-boodschap, klik dan op 'More Info'

![](/images/janban/setup5.png)

Klik op 'Run anyway'

![](/images/janban/setup6.png)

De installatie is nu klaar. Start Outlook en ga naar de 'home' mail folder. Het JanBan bord zal daar automatisch verschijnen. Veel plezier ermee!

![](/images/janban/setup7.png)

