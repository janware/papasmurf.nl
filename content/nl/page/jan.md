---
title: Wie ben ik
date: 2023-04-17
aliases: ['/nl/jan/']
---
Hallo! Mijn naam is Jan van Veldhuizen, maar online heb je me misschien vaker gezien als Papa Smurf.

Mijn liefde voor computers begon al in de jaren 70 toen ik wiskunde studeerde.
Vanaf het moment dat ik voor het eerst met een computer werkte, was ik verkocht.
Het werd mijn passie en vakgebied, waar ik altijd de nieuwste ontwikkelingen ben blijven volgen.
De laatste tijd heeft vooral kunstmatige intelligentie mijn grote interesse. Ik geef er ook [lezingen](../presentations) over.

Al dertig jaar werk ik als programmeur en software architect bij Visma Onguard.
Opensource-software heeft mijn sterke voorkeur.
Een van mijn succesvolste projecten is de Outlook add-in [JanBan](../janban).

Werk en hobby lopen voor mij naadloos in elkaar over.
Thuis in Kortenhoef heb ik mijn eigen server op zolder staan,
waar ik voortdurend mee experimenteer en waar ook deze website op draait.

Naast mijn passie voor computers, houd ik van muziek maken en vreemde talen leren.
Als organist speel ik regelmatig in de kerk.
Talen hebben voor mij een duidelijke link met wiskunde: of het nu programmeer- of gesproken talen zijn,
ik verdiep me graag in woordenboeken[^1], grammatica's en handboeken.

Ik ben getrouwd, vader en opa. [Mijn vrouw](https://jokegoudriaan.nl/over-mij/) en ik zijn fervente wandelaars en fietsers.
Onze langste fietstocht tot nu toe was in 2022, naar Santiago de Compostela.

Deze website is meertalig: sommige pagina's in het Nederlands, mijn moedertaal;
veel in het Engels, omdat dat de universele taal is onder techneuten zoals ik;
en Esperanto, omdat ik dat een zeer geslaagde poging vind tot het creëren van een universele taal.

Over die bijnaam PapaSmurf: een paar jaar geleden bedachten collega's die voor me en sindsdien
staat er een grote-smurf-poppetje[^2] mijn bureau.
Vandaar de naam van deze website.

![Punchcard Terminal](/images/about/punch_reader.jpg)\
een ponskaartenmachine

![Dec Terminal](/images/about/dec-terminal.jpg)\
een papier-terminal

![Book shelf](/images/about/books.jpg)


![Papa Smurf](/images/about/smurf.jpg)
