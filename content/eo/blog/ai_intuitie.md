---
title: AI kontraŭ sperto kaj intuicio
date: 2023-06-01
tags: ['ChatGPT','AI']
thumbnail: /thumbnails/ai_intuition.png
---
ChatGPT kaj aliaj AI-iloj estas nuntempe nepriskribeble necesaj. La aplikaĵoj estas vastaj. Mi uzas ilin ĉiutage en mia laboro kiel programisto. La solvoj kaj sugestoj, kiuj estas proponitaj de tiuj iloj, estas mirindaj. Tamen, ni devas ĉiam esti konsciaj pri tio: nenia ilo povas anstataŭigi nian propran sperton kaj percepton.

Hodiaŭ mi devis solvi problemon pri SQL-skripto. Ĝi faris tion, kion ĝi devis fari, sed ĝi estis malrapida, kvazaŭ testudo. Mi turnis min al ChatGPT kaj petis sugestojn. Estis proponitaj diversaj solvoj, sed ili ne vere helpis min. Iuj solvoj simple ne funkcias, kaj aliaj ne havis la deziratan efikon. La skripto persiste restis malrapida.

Fine, mi flankenigis ChatGPT, rulkis mian maniketon supren, kaj eklaboris. Mi posedas jardekojn da sperto, do tiu problemo estis venkebla. Kaj vere, post nur unu horo da eksperimentado, la skripto estis dekoble pli rapida ol antaŭe.

Tiu sperto ree konfirmis: propra sperto kaj intuicio restas esencaj. AI estas mirinda ilo kaj povas helpi nin kun multaj taskoj. Sed ni ne devas tro multe dependi de ĝi. Ni ankoraŭ restas la regantoj!
