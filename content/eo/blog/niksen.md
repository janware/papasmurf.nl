---
title: Niksen - Tipa nederlanda vorto
date: 2024-02-15
tags: ['Lingvo']
thumbnail: /thumbnails/niksen.avif
---
La nederlanda ŝajnas esti la sola lingvo kun vorto por nenio farado: niksen.

Mi malkovris tion per [ĉi tiu artikolo](https://www.theguardian.com/lifeandstyle/2024/feb/07/the-art-of-doing-nothing-have-the-dutch-found-the-answer-to-burnout-culture).

En la artikolo, Viv Groskop esploras la nederlandan koncepton de niksen, aŭ aktive nenion farado, kiel ebla solvo al brul-elĉerpiĝa kulturo. Ŝi priskribas sian sperton kun niksen ĉe la strando de Scheveningen, kune kun Olga Mecking, aŭtoro de la libro "Niksen: Embracing the Dutch Art of Doing Nothing". Mecking, kiu estas rigardata kiel aŭtoritato pri la temo de niksen, difinas ĝin kiel ion fari sen celo, koncepto kiu altiris internacian atenton. Malgraŭ ĝia populareco kaj la eldono de diversaj libroj pri la temo, niksen ne estas inherente nederlanda nek profunde enradikiĝinta en la nederlanda kulturo, kiu tradicie valoras malmolan laboron kaj produktivecon, en linio kun la kalvinista nacia karaktero.

La artikolo ankaŭ elstarigas la pli larĝan kuntekston de brul-elĉerpiĝo kaj streso en la moderna socio, proponante niksen kiel manieron eskapi la premojn de ĉiam pli postulanta mondo. La intereso en niksen estas parte reago al la moderna vivstilo, kie homoj estas konstante okupitaj kaj havas malmultan tempon por ripozo. Kvankam la koncepto ricevis multe da atento lastatempe, ekspertoj avertas, ke ĝi ne estas panaceo kaj ke ĝia efikeco dependas de la individua alproksimiĝo al rilakso kaj libertempo.
