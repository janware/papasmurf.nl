---
title: Twexit!
date: 2023-07-26
thumbnail: /thumbnails/twexit.jpg
---
Ĉi-semajne, Elon Musk transformis Twitter en X. Ĉu pro kia kaŭzo? Simpliĝinte, pro sia volo. Li kolektis sufiĉe da kapitalo por aĉeti Twitter, kaj nun, tiu retejo estas lian luzaĵon, ion kiun li povas reguli laŭplaĉe. Sed kia estas la opinio de la pli ol 500 milionoj da uzantoj de Twitter? Ĉu li pri tio zorgas?

Mi diras, ke tio jam sufiĉas. Ĉi-matene, mi deaktivigis mian Twitter-konton. Ĉu tio kaŭzos, ke Elon Musk mankas dormon? Verŝajne ne. Kaj mi ankaŭ ne. Kontraŭe.
