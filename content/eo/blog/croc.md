---
title: Croc - how to transfer files between any two systems
date: 2022-12-22
tags: ['Utilities']
thumbnail: /thumbnails/croc.jpg
---
Ĉu vi iam bezonas movi dosieron de via komputilo al fora sistemo? Aŭ eble vi volas kunhavigi dosieron kun kunlaboranto?

Vi povas uzi ilojn kiel ekzemple scp aŭ WinSCP. Se vi uzas Windows Remote Desktop, foje vi povas alglui kaj lasi dosierojn. Sed depende de la situacio, vi bezonas scii la ĝustan manieron transferi dosieron por la specifa konekto, kiun vi uzas.

Tial mi vere ŝatas la transferilon Croc. Ĝi povas movi dosierojn inter du ajnaj komputiloj, kiuj estas konektitaj al la interreto, sendepende de tio, ĉu ili estas malantaŭ fajrovualo. Ĝi funkcias sur Windows, Linux kaj Mac. Ĝi povas trakti tre grandajn dosierojn aŭ eĉ tutaĵojn. Vi simple devas instali Croc-on sur ambaŭ komputiloj.

Mi ne plu devas zorgi pri la disponeblaj metodoj de dosiertransigo. Mi simple pensas: Croc! Mi uzas ĝin hejme por movi dosierojn de unu portatilo al alia. Mi uzas ĝin en la laboro por movi dosierojn al la nubo aŭ inter du sistemoj en la nubo.

Sur la komputilo kun la dosiero, vi tajpas:
```
croc <dosiernomo>
```

Ĝi donas al vi mesaĝon kiel: On the other computer run: croc 3103-magnet-person-mental (la kodo diferencas ĉiun fojon)

Poste, sur la komputilo, kiu ricevas la dosieron, vi simple tajpas tiun komandon:
```
croc 3103-magnet-person-mental
```
Tio estas ĉio. Estas tiel simpla movi ajnan dosieron aŭ dosierujon de ajna grandeco inter ajnaj du komputiloj. Mi amas kiel facila estas!

Por ricevi instrukciojn kaj pli da informo, iru al https://github.com/schollz/croc.
