---
title: Outlook KanBan board aka JanBan
date: 2017-10-02
aliases: ['/janban']
---
![logo](/images/janban/janban-icon-512-300x300.jpg)

[Elŝutligilo](http://www.janware.nl/gitlab/janban.zip)

JanBan Outlook Taskboard estas vidilo en kanban-stila formato por Outlook Taskoj.

Ĉi tiu projekto estas bazita sur [la projekto de Evren Varol](https://github.com/evrenvarol/outlook-taskboard). Mi aldonis multajn funkciojn, personigojn kaj agordan ekranon.

![](/images/janban/scr1.png)

La ĉefaj funkcioj de JanBan estas:
##### Movado de taskoj inter lanoj de taskoj
![](/images/janban/scr2.gif)
##### Filtrado de taskoj
![](/images/janban/scr3.gif)
##### Agordado
![](/images/janban/scr4.gif)
##### Helpo Mailbox
![](/images/janban/scr5.gif)

### Subtenataj platformoj

Mi testis ĝin kun ĉiuj versioj de Outlook de 2013 ĝis nun kaj sur Windows de versio 8 ĝis 11. Ĝi eble ankaŭ funkcias kun pli malnovaj versioj de Outlook kaj verŝajne funkcias kun Windows 7.

Vi ankaŭ povas malfermi la tasktabulon en retumilo. Sed pro kelkaj limigoj kun ActiveX-kontroloj, ĝi funkcias nur en Internet Explorer.

### Agordopcioj

Vi povas uzi JanBan-on en la interreto aŭ agordi ĝin sur via propra komputilo.

Ne gravas, kiel vi uzas ĝin, ĝi funkcios en via Outlook-aplikaĵo, kaj neniu alia vidos viajn Outlook-datumojn.

Jen listo de bonaj kaj malbonaj aferoj pri ĉiu opcio por helpi vin decidi, kiu plej taŭgas por vi:

- Defaŭlta opcio:
  - Agordo: Tre facila
  - Ĝisdatigoj: Ili okazas aŭtomate. Vi ne devas fari ion.
  - Ne konektita: La aplikaĵo ne funkcios, se vi ne estas konektita al la interreto.

- Loka opcio:
  - Agordo: Ĝi postulas iom da laboro.
  - Ĝisdatigoj: Vi devas mem instali ilin.
  - Ne konektita: Vi povas uzi la aplikaĵon eĉ kiam vi ne estas konektita al la interreto.

### Defaŭlta Agordo

Se vi deziras la facilan agordon, simple iru al https://janware.nl/janban kaj faru tion, kion ĝi diras.

Sed se vi volas agordi ĝin mem:

1. Dekkliku vian Outlook-Domonan Dosierujon kaj poste klaku Proprietojn.
2. Elektu la langeton _Home page_ en la aperanta fenestro.
3. En la skatolo kun la etikedo _Address_, tajpu: https://janware.nl/janban.
4. Marku la markobulon, kiu diras _Show home page by default for this folder_, poste klaku OK.

###Lokala Agordo

1. Elŝutu la plej novan ZIP-dosieron de [JanWare](http://www.janware.nl/gitlab/janban.zip) kaj metu ĝin en dosierujon sur via komputilo.
2. Dekkliku vian Outlook-Domonan Dosierujon kaj poste klaku Proprietojn.
3. Elektu la langeton _Home page_ en la aperanta fenestro.
4. En la skatolo kun la etikedo _Address_, trovu la dosierujon, kie vi metis la Taskboard-dosierojn, kaj elektu la kanban.html-dosieron.
5. Marku la markobulon, kiu diras _Show home page by default for this folder_, poste klaku OK.

![](/images/janban/scr6.png)

Se vi vidas ĉi tiun averton, simple klaku sur la ikono de la X por fermi la averton kaj la fenestron de Proprietoj.

### Uzante la instali-lokan komandodosieron

Klaku la elŝutligilon kaj konservu la dosieron en vian Dosierujon "Elŝutoj" (Downloads).

![](/images/janban/setup1.png)

Iru al via Dosierujo "Elŝutoj", dekklaku la dosieron janban.zip kaj elektu "Elpaki ĉion".

![](/images/janban/setup2.png)

Marku la markobulon por montri la elpakitajn dosierojn kaj klaku la butonon "Elpaki".

![](/images/janban/setup3.png)

Duoble klaku la dosieron install.cmd. Vi povas vidi nigra ekranon de komandlinio dum mallonga tempo.

![](/images/janban/setup4.png)

Se Windows montras la mesaĝon pri protekto, klaku sur "Pli da informo".

![](/images/janban/setup5.png)

Klaku sur "Tamen ruli".

![](/images/janban/setup6.png)

La instalo estas preta. Malfermu Outlook-on kaj iru al via plej supra retmesaĝujo. La tabulo de Janban aperos. Ĝuu!

![](/images/janban/setup7.png)
