---
title: Kio mi estas
date: 2023-04-17
---
Saluton! Mi estas Jan van Veldhuizen, sed eble vi pli ofte renkontis min en la reto kiel Papa Smurf.

Mia amo al komputiloj komenciĝis jam en la 1970-aj jaroj, kiam mi studis matematikon.
Depost la unua fojo kiam mi laboris ĉe komputilo, mi tute enamiĝis.
Ĝi fariĝis mia pasio kaj faka kampo, en kiu mi ĉiam sekvis la plej novajn evolujojn.
Lastatempe, precipe artefarita intelekto kaptis mian grandan intereson. Mi eĉ faras [prezentadojn](../presentations) pri ĝi.

Dum la lastaj tridek jaroj mi laboras kiel programisto kaj softvara arkitekto ĉe [Visma Onguard](https://www.onguard.com).
Mi forte preferas malfermitkodajn programojn.
Unu el miaj plej sukcesaj projekto estas la Outlook-aldonaĵo [JanBan](../janban).

Por mi, laboro kaj hobio fluas senprobleme unu en la alian.
Hejme en Kortenhoef mi havas propran servilon sur la subtegmento,
kie mi konstante eksperimentas, kaj kie ankaŭ ĉi tiu retejo funkcias.

Krom mia pasio por komputiloj, mi ŝatas muzikludon kaj lernado de fremdaj lingvoj.
Kiel orgenisto, mi regule ludas en preĝejo.
Lingvoj por mi havas evidentan ligon kun matematiko: ĉu programlingvoj aŭ parolataj lingvoj,
mi ŝatas profundiĝi en vortarojn, gramatikojn kaj lernolibroj.

Mi estas edzo, patro kaj avo. [Mia edzino](https:://jokegoudriaan/nl/over-mij) kaj mi fervore promenas kaj biciklas.
Nia plej longa bicikla vojaĝo ĝis nun estis al Santjago de Kompostelo en 2022.

Ĉi tiu retejo estas plurlingva: kelkaj paĝoj en la nederlanda, mia denaska lingvo;
multaj en la angla, ĉar ĝi estas la universala lingvo inter teknikistoj kiel mi;
kaj Esperanto, ĉar mi rigardas ĝin kiel tre sukcesa provo krei universalan lingvon.

Pri la moknomo PapaSmurf: antaŭ kelkaj jaroj, kolegoj ĝin donis al mi, kaj de tiam
granda Smufo-figureto sidas sur mia laboro.
De tie venas la nomo de ĉi tiu retejo.
![Punchcard Terminal](/images/about/punch_reader.jpg)\
Kartopuncilo

![Dec Terminal](/images/about/dec-terminal.jpg)\
Paperterminalo
![Book shelf](/images/about/books.jpg)

![Papa Smurf](/images/about/smurf.jpg)
