---
title: 'Polpo biciklas en Amsterdamo'
date: 2023-10-04
tags: ['AI']
thumbnail: /AI-image-wk/octopus.jpeg
---

Ĝis hodiaŭ, mi uzis Midjourney por krei bildojn.
Hodiaŭ mi provis la novan DALL-E per Bing.
'Desegnu polpon, kiu biciklas en Amsterdamo,' mi petis.
Estas amuze ke ĝi desegnas mulilon, nederlandan flagon kaj tulipojn por klarigi ke ĝi situas en nederlanda urbo.🌷🇳🇱

![octopus](/AI-image-wk/octopus.jpeg)
