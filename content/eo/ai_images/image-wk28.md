---
title: 'Papa Smurf estas de ferio'
date: 2023-07-15
tags: ['AI']
thumbnail: /AI-image-wk/papasmurf-campsite.png
---

Tiom multe okazis en la nederlanda politiko ekde la lasta AI-bildo. 
La registaro falis, kaj amaso da eminentaj politikistoj anoncis sian retriĝon el la kampo. 
Tio estas perfekta por politika karikaturo. 
Sed sciu ĉi tion? Papo Smurfo estas de ferio! Tio estas tio, kio vere gravas nun... 
Mi ripozas ĉe tendaro en la nordo de Francio. Malstreĉiĝante, kun glaso de vino en mano. 
Kompreneble, la tekokomputilo ankaŭ estas ĉi tie, ĉar oni devas havi iom da ludadtempo, ĉu ne?

![papasmurf-campsite](/AI-image-wk/papasmurf-campsite.png)
