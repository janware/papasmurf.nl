---
title: AI Bildoj de Papa Smurf
---
Kiel fervora uzanto de Midjourney kaj DALL-E, mi decidis regule afiŝi bildojn, kiujn mi kreis per AI. 
Foje, ili estos inspiritaj de aktualaĵoj, estante amuzaj, kritikaj, aŭ ambaŭ. 
Aliaj fojoj, ili simple estos rezultoj de miaj eksperimentoj kun AI-iloj.
