---
title: 'Hundokapo 2.0'
date: 2023-09-24
tags: ['AI']
thumbnail: /AI-image-wk/doghead1.png
---
En la pasinta jarcento, Nederlando havis trajnojn, kiujn oni nomis 'Hundokapo' pro la formo de ilia antaŭparto. 
Lastatempe mi vizitis ekspozicion, kie nova serio de trajnoj estis prezentita sub la nomo 'Hundokapo 2.0'.

<div style="display: flex; justify-content: space-between;">
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead1.png" id="zoom-default" class="medium-zoom-image" aalt="Image 1" style="max-width: 100%; height: auto; display: block;">
    </div>
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead2.png" id="zoom-default" class="medium-zoom-image" aalt="Image 1" style="max-width: 100%; height: auto; display: block;">
    </div>
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead3.png" id="zoom-default" class="medium-zoom-image" aalt="Image 1" style="max-width: 100%; height: auto; display: block;">
    </div>
</div>

La tiutempa hundokapo:
![hondekop](/images/hondekop.jpg) 
