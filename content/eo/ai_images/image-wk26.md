---
title: 'La Eiffel-turo ploras sangon'
date: 2023-07-01
tags: ['AI']
thumbnail: /AI-image-wk/bleeding-eiffeltower.png
---

La pasintan semajnon, la morto de Nahel signis novan malaltan punkton en la historio de rasismo en Francio. Sub malhela ĉielo, la sango gutas de la Eiffel-Turo.

![eiffeltower](/AI-image-wk/bleeding-eiffeltower.png)
