---
title: 'La pluvega de Nov-Jorko'
date: 2023-10-02
tags: ['AI']
thumbnail: /AI-image-wk/newyork.png
---

Ĝi vere pluvas tre severe en Nov-Jorko lastatempe. [NBC News](https://www.nbcnews.com/science/environment/nyc-flooding-climate-change-infrastructure-limitations-rcna118170)
Eĉ la torĉo de la fama Statuo de Libereco estis anstataŭigita per ombrelo. Tamen, la ombrelo apenaŭ povas elteni la ŝtormon.

![newyork](/AI-image-wk/newyork.png)

