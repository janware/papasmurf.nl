---
title: 'Running for President'
date: 2023-06-04
tags: ['AI']
thumbnail: /AI-image-wk/running-for-president.png
---
Pasintsemajne, novaĵo disvastiĝis pri Joe Biden [falanta](https://www.bbc.com/news/world-us-canada-65783589) kiam li eniris la scenejon. La reago de Donald Trump estis mirinde afabla, dirante: "Mi esperas, ke li ne vundiĝis". Tamen, ambaǔ ankoraǔ estas en la kuro por prezidanteco. Mi kreis tiun bildon per Midjourney kaj donis al ĝi la titolon: 'Running for President'.

![Running for President](/AI-image-wk/running-for-president.png)
