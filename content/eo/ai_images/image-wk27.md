---
title: 'Tutmonda Fandegiĝo'
date: 2023-07-06
tags: ['AI']
thumbnail: /AI-image-wk/earth-melting.png
---

Ĉi-semajne, ni atingis tragedian punkton en klimatŝanĝo: [la plej varma tago](https://www.washingtonpost.com/climate-environment/2023/07/05/hottest-day-ever-recorded/) iam registrita tutmonde. Dum ni ĝuas niajn glaciaĵojn, la terglobo mem fandegiĝas simile al na kulero da fandanta glacio...

![melting earth](/AI-image-wk/earth-melting.png)
