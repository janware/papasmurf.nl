---
title: 'La "Cloud": ĉu ĝi estas nur la komputilo de via najbaro??'
date: 2023-11-12
tags: ['AI']
thumbnail: /AI-image-wk/cloud.png
---

![cloud](/AI-image-wk/cloud.png)

"Ne ekzistas nubo. Ĝi estas nur la komputilo de iu alia."
Tiu frazo ofte estas uzata por klarigi 'la Nubon'. 
Unuflanke, tio estas vera; ĝi fakte estas kolekto de komputiloj en datumcentroj 
tra la tuta mondo. 
Tamen, ĝi ankaŭ estas mia propra komputilo en mia tegmento. 
Aliflanke, la nubo estas koncepto kiu transiras la nuran prunton de la komputilo de via najbaro.
La nubo estas kiel tunelo, enirejo al potenca, kunhavata komputado, 
permesante al vi uzi kaj konservi datumojn multe preter la limoj de via propra aparataro

