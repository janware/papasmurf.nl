---
title: 'La nova laboro de Boris Johnson'
date: 2023-06-15
tags: ['AI']
thumbnail: /AI-image-wk/boris-johnson.png
---

Post esti elpelita el la Parlamento, Boris Johnson trovis por si novan laboron la tujan sekvan tagon kiel kolumnisto por la Daily Mail. Oni povus miri, kial li ne antaŭe sekvis tiun vojon. Li havas talenton por verki humorplenajn artikolojn. Tio ŝajnas multe malpli danĝera ol klaŭneca entrepreno kiel Brexit.

![Boris Johnson](/AI-image-wk/boris-johnson.png)
