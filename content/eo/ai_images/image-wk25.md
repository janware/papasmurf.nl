---
title: 'Senpagaj kolbasobuloj por la loǧantoj de Moskvo'
date: 2023-06-25
tags: ['AI']
thumbnail: /AI-image-wk/hotdogs.png
---

Loĝantoj de Moskvo havos liberan tagon ĉi-lundon, komence pro tio, ke la armeo de la eksa hotdog-vendisto, Prigozhin, alproksimiĝis al la urbo. Ŝajnas, ke tiu minaco nun estas evitita. Tamen, lundo restos libera tago. Ĉiuj civitanoj estas invititaj al la Ruĝa Placo por ricevi senpagan kolbasobulkon.

![hot dogs](/AI-image-wk/hotdogs.png)
