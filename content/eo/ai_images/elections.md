---
title: 'Nederlanda elektado 2023'
date: 2023-11-22
tags: ['AI']
thumbnail: /AI-image-wk/elections.png
---

![elections](/AI-image-wk/elections.png)

Hodiaŭ estas elektago en Nederlando. 
La lando ŝajnas esti pli dividita ol iam ajn antaŭe. 
Estas tiom da partioj... kaj eĉ ne malpli ol kvar el ili povus fariĝi la plej grandaj per nur kelkaj voĉoj de diferenco. 
La televida debato ŝajnas pli esti batalo pri povo inter la maldekstro kaj dekstro ol vera diskuto pri la enhavo. 
Espereble, ekde morgaŭ, okazos kunlaboro anstataŭ plua disdividado.

