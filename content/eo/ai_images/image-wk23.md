---
title: 'Messi konstruas sablaj kasteloj'
date: 2023-06-09
tags: ['AI']
thumbnail: /AI-image-wk/poor-messi.png
---

Lionel Messi pli ŝatas sablokastelojn ĉe Miamo-Plaĝo ol orajn montojn en Sauda Arabio. Mizeraj 120 milionoj da eŭroj estis sufiĉaj por lin decidi iri al la Sunbrila Ŝtato.

![Poor Messi](/AI-image-wk/poor-messi.png)
