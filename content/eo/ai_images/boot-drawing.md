---
title: 'Vivanta en boto'
date: 2023-11-05
tags: ['AI']
thumbnail: /AI-image-wk/boot-drawing.png
---

![jigsaw](/AI-image-wk/puzzel.png)

Antaŭ pli ol tridek jaroj, mi kreis puzlon por unu el miaj infanoj uzante desegnon, kiun mi mem faris.

Nun, mi faris similan bildon per AI, kaj ĝi estis preta en nur kelkaj minutoj.

![boot](/AI-image-wk/boot-drawing.png)
