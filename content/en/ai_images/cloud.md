---
title: 'The Cloud: is it just your neighbor''s computer?'
date: 2023-11-12
tags: ['AI']
thumbnail: /AI-image-wk/cloud.png
---

![cloud](/AI-image-wk/cloud.png)

"There is no cloud. It is just someone else's computer."
This is often said to demistify 'the Cloud'. 
On one hand this is true; it's just a bunch of computers in data centers around the globe. 
However, it's also my own computer in my attic. On the other hand, the cloud is a concept
that is more than borrowing your neighbor's computer. 
The cloud is like a tunnel, a gateway to powerful, shared computing, letting
you use and store data far beyond your own hardware's limits.
