---
title: 'Boris Johnson''s new job'
date: 2023-06-15
tags: ['AI']
thumbnail: /AI-image-wk/boris-johnson.png
---

After being dismissed from Parliament, Boris Johnson found a new job the very next day - a columnist for the Daily Mail. One might wonder why he didn't pursue this path earlier. He's pretty good at writing funny articles. Arguably, this could have been a more suitable role for him than being a political clown in the Brexit spectacle.

![Boris Johnson](/AI-image-wk/boris-johnson.png)
