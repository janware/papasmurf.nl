---
title: 'Running for President'
date: 2023-06-04
tags: ['AI','image-of-the-week']
thumbnail: /AI-image-wk/running-for-president.png
---

Last week, news broke that Joe Biden [stumbled](https://www.bbc.com/news/world-us-canada-65783589) while ascending the stage. Surprisingly, Donald Trump's reaction was rather gracious, as he said, "I hope he wasn't hurt". Regardless, both remain in the race. I generated this image with Midjourney and titled it: "Running for President".

![Running for President](/AI-image-wk/running-for-president.png)
