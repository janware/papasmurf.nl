---
title: 'Global Meltdown'
date: 2023-07-06
tags: ['AI']
thumbnail: /AI-image-wk/earth-melting.png
---

This week, we reached a tragic milestone in climate change: the [hottest day ever](https://www.washingtonpost.com/climate-environment/2023/07/05/hottest-day-ever-recorded/) recorded worldwide. While we are enjoying our ice creams, the globe itself is melting just like that same scoop of ice…

![melting earth](/AI-image-wk/earth-melting.png)
