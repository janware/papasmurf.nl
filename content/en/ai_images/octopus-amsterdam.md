---
title: 'Octopus biking in Amsterdam'
date: 2023-10-04
tags: ['AI']
thumbnail: /AI-image-wk/octopus.jpeg
---

Until today, I have been using Midjourney to create images.
Today I tried the new DALL-E via Bing.
'Draw an octopus riding a bike in Amsterdam,' I requested.
It is funny that it draws a windmill, a Dutch flag, and tulips to make clear that it's set in a Dutch city. 🌷🇳🇱

![octopus](/AI-image-wk/octopus.jpeg)
