---
title: 'Dutch elections 2023'
date: 2023-11-22
tags: ['AI']
thumbnail: /AI-image-wk/elections.png
---

![elections](/AI-image-wk/elections.png)

Today is election day in the Netherlands. 
The country seems more divided than ever before. 
So many parties... and no less than four parties could become the largest with only a few votes difference. 
The debate on television is more a power struggle between left and right than a discussion about content. 
Hopefully, from tomorrow, there will be more cooperation and less division.
