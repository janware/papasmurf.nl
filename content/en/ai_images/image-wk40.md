---
title: 'New York''s Deluge'
date: 2023-10-02
tags: ['AI']
thumbnail: /AI-image-wk/newyork.png
---

It has been raining quite severely in New York lately. [NBC News](https://www.nbcnews.com/science/environment/nyc-flooding-climate-change-infrastructure-limitations-rcna118170)
Even the torch of the famous Statue of Liberty has been replaced with an umbrella, but the umbrella itself can hardly withstand the storm.

![newyork](/AI-image-wk/newyork.png)
