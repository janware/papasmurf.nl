---
title: 'Messi is going to play at Miami Beach'
date: 2023-06-09
tags: ['AI']
thumbnail: /AI-image-wk/poor-messi.png
---

Lionel Messi is trading the golden moutains of Saudi Arabia for the sandcastles of Miami Beach. A modest sum of 120 million euros was enough to convince him to make the Sunshine State his new home.

![Poor Messi](/AI-image-wk/poor-messi.png)
