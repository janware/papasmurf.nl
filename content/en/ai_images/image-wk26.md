---
title: 'The Eiffel Tower is weeping blood'
date: 2023-07-01
tags: ['AI']
thumbnail: /AI-image-wk/bleeding-eiffeltower.png
---

Last week marked a tragic chapter in the history of racism in France with the death of Nahel. Under a somber sky, the Eiffel Tower appears to weep blood.

![eiffeltower](/AI-image-wk/bleeding-eiffeltower.png)
