---
title: 'Free Hot Dogs for Moscow Residents'
date: 2023-06-25
tags: ['AI']
thumbnail: /AI-image-wk/hotdogs.png
---

Residents of Moscow will have a day off this coming Monday. Originally, the holiday was declared due to an advancing threat from the army of Prigozhin, a former hotdog vendor. This threat now appears to have been averted. Regardless, Monday will remain a day off. All citizens are invited to the Red Square to receive a free hotdog.

![hot dogs](/AI-image-wk/hotdogs.png)
