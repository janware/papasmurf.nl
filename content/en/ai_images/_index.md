---
title: Papa Smurf's AI Image Gallery
---
As an enthusiastic user of both Midjourney and DALL-E, I've decided to regularly share pictures that I've created using AI. 
Sometimes, these images will be inspired by current news—whether they're humorous, critical, or a blend of both. 
At other times, they'll simply be the outcome of my experiments with AI tools.
