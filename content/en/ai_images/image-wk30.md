---
title: 'Bird’s Eye View on Twitter X'
date: 2023-07-24
tags: ['AI']
thumbnail: /AI-image-wk/eggs.png
---

Elon Musk has decided to rename his Twitter platform to 'X'. 
Doesn't he realize that he can't have eggs without having a bird in the first place? 
The Twitter bird thinks to itself, 'Humph! The boss wants X? Well, here they are. I hope he chokes on them.'

![eggs](/AI-image-wk/eggs.png)
