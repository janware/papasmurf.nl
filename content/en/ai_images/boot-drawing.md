---
title: 'Living in a boot'
date: 2023-11-05
tags: ['AI']
thumbnail: /AI-image-wk/boot-drawing.png
---

![jigsaw](/AI-image-wk/puzzel.png)

Over thirty years ago, I created a jigsaw puzzle for one of my children using a drawing I had made myself.

Now, I've made a similar image using AI, and it was ready in just a matter of minutes.

![boot](/AI-image-wk/boot-drawing.png)
