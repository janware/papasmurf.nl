---
title: 'Doghead 2.0'
date: 2023-09-24
tags: ['AI']
thumbnail: /AI-image-wk/doghead1.png
---
In the past century, the Netherlands had trains that were nicknamed 'Doghead' due to the shape of their fronts. 
I recently attended an exhibition where a new series of train sets was unveiled, named 'Doghead 2.0'.

<div style="display: flex; justify-content: space-between;">
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead1.png" alt="Image 1" id="zoom-default" class="medium-zoom-image" astyle="max-width: 100%; height: auto; display: block;">
    </div>
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead2.png" alt="Image 1" id="zoom-default" class="medium-zoom-image" astyle="max-width: 100%; height: auto; display: block;">
    </div>
    <div style="flex: 1;padding: 0 10px;">
        <img src="/AI-image-wk/doghead3.png" alt="Image 1" id="zoom-default" class="medium-zoom-image" astyle="max-width: 100%; height: auto; display: block;">
    </div>
</div>

The old Doghead:
![hondekop](/images/hondekop.jpg) 
