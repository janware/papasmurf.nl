---
title: 'Papa Smurf is on vacation'
date: 2023-07-15
tags: ['AI']
thumbnail: /AI-image-wk/papasmurf-campsite.png
---

So much has happened in Dutch politics since the last AI image I posted. 
The cabinet collapsed, and lots of high-profile politicians announced their departure from the field. 
It's ripe for a political cartoon. But you know what? Papa Smurf is on vacation! 
That's what I find really important right now...
I'm chilling at a campsite in the north of France. Laid back, with a glass of wine in hand. 
Of course, the laptop is here too, because you gotta have some playtime, right?

![papasmurf-campsite](/AI-image-wk/papasmurf-campsite.png)
