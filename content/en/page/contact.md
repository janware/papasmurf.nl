---
title: "Contact Form"
hideMeta: true
toc: false
---
<section class="bg-white dark:bg-gray-800">
  <div class="py-8 lg:py-16 px-4 mx-auto max-w-screen-md">
      <form method="post" class="space-y-2" action="https://forms.un-static.com/forms/2e5e33ae15312b870861eef88a681f25f36fc6b4">
          <div class="form-group">
              <label for="name" class="text-lg mb-1">
                 Name
              </label>
              <input type="name" 
                     id="name" 
                     name="name"
                     class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light" 
                     required>
          </div>
          <div class="form-group">
              <label for="email" class="text-lg mb-1">
                 Email
              </label>
              <input type="email"
                     id="email"
                     name="email"
                     class="shadow-sm bg-gray-50 border border-gray-300 text-gray-900 text-lg rounded-lg focus:ring-primary-500 focus:border-primary-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                     required>
          </div>
          <div class="form-group">
              <label for="subject" class="text-lg mb-1">
                 Subject
              </label>
              <input type="text" 
                     id="subject" 
                     name="subject"
                     class="block p-3 w-full text-lg text-gray-900 bg-gray-50 rounded-lg border border-gray-300 shadow-sm focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500 dark:shadow-sm-light"
                     required>
          </div>
          <div class="form-group sm:col-span-2">
              <label for="message" class="text-lg mb-1">
                 Message
              </label>
              <textarea id="message" 
                     name="message"
                     rows="3" 
                     class="block p-2.5 w-full text-lg text-gray-900 bg-gray-50 rounded-lg shadow-sm border border-gray-300 focus:ring-primary-500 focus:border-primary-500 dark:bg-gray-700 dark:border-gray-600 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500">
              </textarea>
          </div>
          <div class="form-group">
              <span>&nbsp;</span><br>
              <button type="submit" 
                      class="bg-transparant border border-gray-300 dark:border-gray-500 py-3 px-5 text-lg font-medium text-center rounded-lg bg-primary-700 sm:w-fit hover:bg-gray-300 dark:hover:bg-gray-700 focus:ring-4 focus:outline-none focus:ring-primary-300 dark:bg-primary-600 dark:hover:bg-primary-700 dark:focus:ring-primary-800">
                      &nbsp;&nbsp;Send&nbsp;&nbsp
              </button>
          </div>
      </form>
  </div>
</section>
