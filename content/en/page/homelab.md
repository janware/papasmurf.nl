---
title: Homelab
date: 2023-04-01
---
**This page is still under construction**

```
| Device       | Model                     | Type | Capacity | Partition      | LVM Type     | Mount Point     |
|--------------|---------------------------|------|----------|----------------|--------------|-----------------|
| /dev/nvme0n1 | Samsung NVMe              | SSD  | 1Tb      | /dev/nvme0n1p1 | 2Tb Striped  | /nvme (1)       |
| /dev/nvme1n1 | Samsung NVMe              | SSD  | 1Tb      | /dev/nvme1n1p1 |              | /nvme (2)       |
| /dev/sda     | Seagate ST2000VN004-2E41  | HDD  | 2Tb      | /dev/sda1      | 2Tb RAID1    | /data2 (1)      |
| /dev/sde     | Seagate ST2000LM007-1R81  | HDD  | 2Tb      | /dev/sde1      |              | /data2 (2)      |
| /dev/sdd     | Samsung SSD 870           | SSD  | 4Tb      | /dev/sdd1      | 8Tb Striped  | /ssd (1)        |
| /dev/sdf     | Crucial MX500 4TB         | SSD  | 4Tb      | /dev/sdf1      |              | /ssd (2)        |
| /dev/sdb     | Samsung ST1000LM024 HN-M  | HDD  | 1Tb      | /dev/sdb1      |              | /data1          |
| /dev/sdc     | Samsung SSD 870           | SSD  | 250Gb    | /dev/sdc1      |              | (grub)          |
|              |                           |      |          | /dev/sdc2      |              | /boot/efi       |
|              |                           |      |          | /dev/sdc3      |              | /               |
```
