---
author: Jan van Veldhuizen 
title: Who am I
date: 2023-04-17
---

Hello! I am Jan van Veldhuizen, but you might have met me as Papa Smurf on the internet.

My love for computers began back in the 1970s when I studied mathematics.
From the moment I first worked with a computer, I was hooked.
It became my passion and field of expertise, where I have continued to follow the latest developments.

Lately, artificial intelligence has been my primary interest. I even give [lectures](../presentations) about it.

For thirty years, I have worked as a programmer and software architect at [Visma Onguard](https://www.onguard.com).
Open-source software is my strong preference.
One of my most successful projects is the Outlook add-in [JanBan](../janban).

Work and hobby seamlessly blend together for me.
At home in Kortenhoef, I have my [own server](../homelab) in the attic, where I constantly experiment and where this website also runs.

Aside from my passion for computers, I enjoy making music and learning foreign languages. As an organist, I regularly play at church.
Languages have a clear link to mathematics for me: whether programming or spoken languages, I enjoy delving into dictionaries, grammars, and manuals.

I am married, a father, and a grandfather. [My wife](https://jokegoudriaan.nl/over-mij) and I are avid hikers and cyclists.
Our longest cycling trip so far was to Santiago de Compostela in 2022.

This website is multilingual: some pages in Dutch, my native language; many in English, as it is the universal language among techies like me;
and Esperanto, which I find a very successful attempt at creating a universal language.

About that nickname Papa Smurf: a few years ago, colleagues came up with it for me, and since then, a Papa Smurf figure has stood on my desk.
Hence the name of this website.

![Punchcard Terminal](/images/about/punch_reader.jpg)  
Image: a punchcard writer

![Dec Terminal](/images/about/dec-terminal.jpg)  
Image: a paper terminal  


![Book shelf](/images/about/books.jpg)  


![Papa Smurf](/images/about/smurf.jpg)
