---
title: APL - A Programming Language
date: 2024-03-01
tags: ['Programming']
thumbnail: /thumbnails/aplkbd.jpg
---
Today, I was reminded of one of the first programming languages I learned in the seventies: APL.

A Visma colleague from Riga, Latvia, pinged me this morning, saying, "Hey Jan, I've read somewhere
that you know about APL. Would you be able to help us with a piece of software that is written
in APL in the seventies? It is still operational and we have some questions about it."
Wow... that's what you call legacy software!

But yes, I learned APL while studying mathematics in the seventies. I wrote my first lines of APL
on a [paper terminal](/images/about/dec-terminal.jpg).

APL, literally means 'A Programming Language'. The simplicity of this name is in stark contrast
with how it looks, a bit like [hieroglyphs](/images/apl.png), and the keyboard is complicated too,
with APL characters even [on the side of some keys](/thumbnails/aplkbd.jpg).

Initially designed as a mathematical notation system around 1960, with a strong focus on working
with multidimensional arrays, it soon turned out to be suitable as a programming language.
One of the strength of it is its conciseness. I mean, the implementation of the
Sieve of Eratosthenes to generate prime numbers can be programmed in
[a single line of APL](https://old.aplwiki.com/SieveOfEratosthenes), whereas in languages like C or Python,
you would need dozens of lines.
No wonder I soon became addicted to it. The result was that I wasn't found in the lecture halls
anymore because I was playing around with APL at the university computer center.

Hence, my math study failed. But the knowledge of this programming language led to my first job as an APL programmer
at the ABN Bank in Amsterdam. And that's how my career in IT began.

