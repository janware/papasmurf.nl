---
title: Croc - how to transfer files between any two systems
date: 2022-12-22
tags: ['Utilities']
thumbnail: /thumbnails/croc.jpg
---
Ever needed to transfer a file from your computer to a remote system? Or perhaps you've wanted to share a file with a colleague?

You could employ tools like scp or winscp. If you're using Windows Remote Desktop, you can sometimes drag and drop files. However, depending on the situation, you need to determine the correct file transfer method for the specific connection you're using.

This is why I'm a huge fan of the Croc transfer tool. It enables the movement of files between any two computers connected to the internet, even if they're behind firewalls. Croc is compatible with Windows, Linux, and Mac. It can handle large files or even entire directories. The only requirement is that Croc needs to be installed on both systems.

No more worrying about the type of file transfer methods available. Now, I simply think: Croc! I use it at home to move files from one laptop to another, and at work to transfer files to the cloud or between two cloud systems.

On the computer containing the file, you simply type:
```
croc <filename>
```

The response you receive will look something like this: 'On the other computer, run: croc 3103-magnet-person-mental'. Note that the code varies each time.

Then, on the computer set to receive the file, all you do is enter that command:
```
croc 3103-magnet-person-mental
```

And that's all there is to it! It's remarkably simple to move any file or folder of any size between any two computers. I love how easy it is!

For instructions and additional information, visit https://github.com/schollz/croc.
