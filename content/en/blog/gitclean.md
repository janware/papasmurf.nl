---
title: Cleaning Up Your Local Git Repository
date: 2024-04-24
tags: ['Tips & Tricks', 'Development', 'Git']
thumbnail: /thumbnails/git-clean.png
---
As a developer, you might have numerous branches in your local Git repository that you've been working on for various features, bug fixes, or experiments. However, over time, some of these branches may have already been merged and deleted on the remote server, but they still linger in your local development folder. These orphan branches can clutter your workspace and make it harder to navigate through your repository. In this blog post, we'll explore why it's important to clean up your local Git repository and how you can efficiently remove orphan branches using a handy Git alias.

## Why Clean Up Your Local Git Repository?

1. **Improved Organization**: Removing orphan branches helps keep your local repository organized and tidy. It eliminates the confusion caused by having numerous stale branches that are no longer relevant to your current work.

2. **Reduced Clutter**: When you have many branches in your local repository, it can be challenging to find the ones you're actively working on. By cleaning up orphan branches, you reduce the visual clutter and make it easier to focus on the branches that matter.

3. **Faster Navigation**: With fewer branches in your local repository, navigating between branches becomes quicker and more efficient. You won't have to scroll through a long list of branches to find the one you need.

4. **Freeing Up Space**: Although branches don't occupy a significant amount of disk space, removing orphan branches can still help free up some storage, especially if you have a large number of them.

## Removing Orphan Branches Step by Step

Let's walk through the process of removing orphan branches from your local Git repository using a step-by-step example.

Suppose you have a local repository with the following branches:
```
$ git branch
feature/login
feature/search

master
experiment/new-ui
bugfix/authentication
```
1. **Fetch Updates from the Remote Repository**:
   First, you need to fetch the latest updates from the remote repository to ensure you have the most up-to-date information about the branches. Run the following command:
```
$ git fetch -p
```
The `-p` flag stands for "prune," which removes any remote-tracking references that no longer exist on the remote.

2. **Identify Orphan Branches**:
Next, you can identify the orphan branches by running the following command:
```
$ git branch -vv | grep ': gone]'
feature/search         abcd123 [origin/feature/search: gone] Add search functionality
experiment/new-ui      ef45678 [origin/experiment/new-ui: gone] Experiment with new UI design
<ScrollWheelDown
```
This command lists all the branches in your local repository along with their remote-tracking references. The `grep ': gone\]'` part filters the output to show only the branches whose remote-tracking references no longer exist.

3. **Delete Orphan Branches**:
Now that you have identified the orphan branches, you can delete them one by one using the `git branch -D` command followed by the branch name. For example:
```
$ git branch -D feature/search
Deleted branch feature/search (was abcd123).
$ git branch -D experiment/new-ui
Deleted branch experiment/new-ui (was ef45678).
```
## Creating a Git Alias for Cleaning Orphan Branches

To make the process of cleaning orphan branches even more convenient, you can create a Git alias that combines all the steps into a single command. Here's an example of an alias command you can add to your local Git configuration:
```
[alias]
clbr = "!f() { git fetch -p && git branch -vv | grep ': gone]' | awk '{print $1}' | while read branch; do echo Deleting branch $branch; git branch -D $branch; done; }; f"
```
With this alias in place, you can simply run `git clbr` in your terminal, and it will automatically fetch updates, identify orphan branches, and delete them for you.

Let's break down what each part of the alias command does:

1. `git fetch -p`: Fetches the latest updates from the remote repository and prunes any remote-tracking references that no longer exist.
2. `git branch -vv`: Lists all the branches in your local repository along with their remote-tracking references.
3. `grep ': gone\]'`: Filters the output to show only the branches whose remote-tracking references no longer exist.
4. `awk '{print $1}'`: Extracts the branch names from the filtered output.
5. `while read branch; do echo Deleting branch $branch; git branch -D $branch; done;`: Iterates over each orphan branch, prints a message indicating the branch being deleted, and then deletes the branch using `git branch -D`.

By using this Git alias, you can clean up your local Git repository with a single command, making the process quick and efficient.

## Conclusion

Cleaning up your local Git repository by removing orphan branches is a good practice to maintain a tidy and organized workspace. By following the step-by-step guide and utilizing the provided Git alias, you can easily identify and delete orphan branches, keeping your repository clutter-free and focused on the branches that matter. Incorporate this cleanup process into your regular Git workflow to ensure a clean and efficient development environment.

