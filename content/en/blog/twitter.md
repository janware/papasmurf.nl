---
title: Twexit!
date: 2023-07-26
thumbnail: /thumbnails/twexit.jpg
---
This week, Elon Musk changed the name of Twitter to X. Why? Simply because he could. He has amassed enough wealth to acquire Twitter, and now, it's his plaything—something he can manipulate at will. But what do the over 500 million Twitter users think about this? Does he even care?

Enough is enough. This morning, I deactivated my Twitter account. Will this cause Elon Musk to lose sleep? Probably not. And neither will I. Quite the contrary.
