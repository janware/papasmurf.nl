---
title: AI versus Expertise and Intuition
date: 2023-06-01
tags: ['ChatGPT','AI']
thumbnail: /thumbnails/ai_intuition.png
---
ChatGPT and other AI tools are indispensable these days. Their applications are numerous. I use them daily in my work as a software developer, and the solutions and suggestions they provide are remarkable. However, we must always remember: no tool can replace our own experience and insight.

Today, I faced a challenge with an SQL script. It was functioning as intended, but far too slowly. It was like a turtle. I turned to ChatGPT and asked for suggestions. A number of solutions were proposed, but none seemed to help. Some simply didn't work, while others failed to produce the desired effect. The script remained stubbornly slow.

In the end, I put ChatGPT aside, rolled up my sleeves, and tackled the problem head-on. With decades of experience under my belt, I knew this problem could be resolved. And indeed, after an hour of hard work, the script was running ten times faster than before.

This experience reaffirmed that our own expertise and intuition are still essential. AI is a wonderful tool that can alleviate much of our workload, but we shouldn't become overly reliant on it. After all, we are the ones in control!