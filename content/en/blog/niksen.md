---
title: Niksen - A typical Dutch word
date: 2024-02-15
tags: ['Language']
thumbnail: /thumbnails/niksen.avif
---
Dutch appears to be the only language with a word for doing nothing: niksen.

I found this out through [this article](https://www.theguardian.com/lifeandstyle/2024/feb/07/the-art-of-doing-nothing-have-the-dutch-found-the-answer-to-burnout-culture).

In the article, Viv Groskop investigates the Dutch concept of niksen, or actively doing nothing, as a possible solution to burnout culture. She describes her experience with niksen on the beach of Scheveningen, together with Olga Mecking, author of the book "Niksen: Embracing the Dutch Art of Doing Nothing". Mecking, who is seen as an authority on the subject of niksen, defines it as doing something without a purpose, a concept that has attracted international attention. Despite its popularity and the publication of various books on the subject, niksen is not inherently Dutch nor deeply rooted in Dutch culture, which traditionally values hard work and productivity, in line with the Calvinist national character.

The article also highlights the broader context of burnout and stress in modern society, proposing niksen as a way to escape the pressures of an increasingly demanding world. The interest in niksen is partly a reaction to the modern lifestyle, where people are constantly busy and have little time for relaxation. Although the concept has received a lot of attention recently, experts warn that it is not a panacea and that its effectiveness depends on the individual's approach to relaxation and leisure.
