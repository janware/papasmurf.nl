---
title: Fixing Time Span Issues with a Time Zone Trick
date: 2024-01-28
tags: ['Timezones', 'Tips & Tricks']
thumbnail: /thumbnails/timezones.webp
---
When setting up an Azure Build pipeline to run outside business hours, I hit a limitation when I wanted it to operate between 5 PM to 8 AM. 
The issue arises because the system's input validator doesn't handle time spans that cross midnight. 

It reads the setup as invalid since it interprets 5 PM (17:00) as being after 8 AM (08:00) within the same day, which logically, it is.
In my case, I had this issue adding a time restriction for an Azure pipeline, but you might have a similar issue defining a time span somewhere else.

Here's a straightforward solution to this problem: use time zones to your advantage. 
The validator doesn't recognize the concept of a day changing at midnight for these time spans. However, by shifting the time zone, you can work around this limitation.

In my case, converting the times to a different time zone where the start time comes before the end time, even when crossing midnight, did the trick. 
By setting the task to run between 01:00 JST and 16:00 JST, I effectively aligned it with my desired window of 17:00 CET to 08:00 CET. 
This change makes the validator see the times as valid because, in JST, 01:00 indeed comes before 16:00.

This workaround is simple, doesn't require any system alterations, and ensures your pipeline runs within the desired timeframe. 
So, if you're stuck with time span restrictions that cross midnight, just pick a time zone where your start and end times fit the system's expectations. 

Problem solved.
