---
title: Add a Contact Form to an Mkdocs-Material Website
date: 2024-02-06
tags: ['Mkdocs-Material']
thumbnail: /thumbnails/form8.png
---
Adding an input form to your static website typically requires scripting and a mechanism to capture the input. 
[Un-static](https://un-static.com) offers a simple and straightforward solution to this challenge. 
Let's dive into how you can implement it on your site.

The instructions on Un-static are so straightforward that additional explanation is hardly needed. 
However, for clarity, let's walk through the process.

Firstly, you need to create two files on your website: a form and a confirmation screen.

In this example, I've created a basic contact form:

<img src="/images/unstatic/form8.png" style="border: 2px solid black; padding: 4px;">

And for the example, a simple confirmation screen:

<img src="/images/unstatic/form7.png" style="border: 2px solid black; padding: 4px;">

Now, head over to the [Un-static website](https://un-static.com). 
For the simplest implementation, you don't even need to create an account. 
Just click on 'Register form'.

<img src="/images/unstatic/form1.png" style="border: 2px solid black; padding: 4px;">

Note: This is the most basic implementation. 
Creating an account with Un-static offers more advanced possibilities for handling your form.

You'll need to provide the email address where Un-static can send the form contents, and the URL users should be redirected to after submitting the form. 

<img src="/images/unstatic/form2.png" style="border: 2px solid black; padding: 4px;">


After completing the activation process via email, 
you'll see a screen summarizing your form's details. 
The most important part of this screen is the link provided at the bottom.

<img src="/images/unstatic/form4.png" style="border: 2px solid black; padding: 4px;">

Copy the action attribute value and incorporate it into the form in your `contact.md` page. 
That's essentially all the setup required.

When testing the form:

<img src="/images/unstatic/form5.png" style="border: 2px solid black; padding: 4px;">

You'll notice a CAPTCHA screen appears to verify if you're human. 
This step might seem inconvenient, but it effectively prevents bots from abusing your form. 
If you have an Un-static account, you can disable this feature if you prefer risking your mailbox to potential spam. 
Following this, you'll be directed to the callback screen with the confirmation.

The data entered in the form will be sent directly to your mailbox.

<img src="/images/unstatic/form9.png" style="border: 2px solid black; padding: 4px;">

That's it!<br>
You can view the live example [here](https://contact-form.papasmurf.nl/contact).
