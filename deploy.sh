#!/bin/bash

# Server details
SERVER="papasmurf.nl"
SSH_USER="ftp@papasmurf.nl"

echo -n "Enter SSH password: "
read -s SSH_PASS
echo

# Local directory to upload
LOCALDIR="./public"

# Remote directory to upload files
REMOTEDIR="."

hugo
lftp -u $SSH_USER,$SSH_PASS $SERVER <<EOF
set ftp:ssl-allow no
mirror -Re -P5 $LOCALDIR $REMOTEDIR
bye
EOF
